/*
  gyrus-admin-private.c

  GYRUS -- GNOME Cyrus Administrator. Administrator Class.
  
  Copyright (C) 2007 Claudio Saavedra <csaavedra@alumnos.utalca.cl>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
  
*/

#ifndef __GYRUS_ADMIN_PRIVATE__
#define __GYRUS_ADMIN_PRIVATE__

#include "gyrus-admin.h"

G_BEGIN_DECLS

/*** Gyrus Administrator Private Data ***/
struct _GyrusAdminPrivate {
	GtkWidget *label_host;
	GtkWidget *label_user;
	GtkWidget *label_port;
/*	GtkWidget *entry_pass; */
	GtkWidget *button_connect;

	GtkWidget *label_mailbox_owner;
	GtkWidget *label_quota_limit;
	GtkWidget *label_quota_free;
	GtkWidget *entry_quota_new;
	GtkWidget *button_quota_apply;
	
	GtkWidget *entry_find_account;
	GtkWidget *window_find_account;
	
	GtkWidget *button_acl_apply;
	GtkWidget *button_acl_add;
	GtkWidget *button_acl_remove;
	GtkWidget *button_close_page;
	
	GtkWidget *button_user_create;
	GtkWidget *button_user_remove;
       	
	GtkWidget *label_mailbox_name;
	GtkWidget *label_page;

	GtkWidget *expander_modify_quota;
	GtkWidget *expander_modify_acl;
	
	GtkTreeView *treeview_users;
	GtkTreeView *treeview_acl;
	GtkTreeView *treeview_corrupt_mailbox;

	GtkWidget *label_acl;
	GtkWidget *scrolled_acl;
	
	GSocketConnection *connection;
	GSocketClient *tcp_socket;
	gchar *buffer;

	GyrusSession *session;
};

G_END_DECLS

#endif /* __GYRUS_ADMIN_PRIVATE_H__ */
