/*
  gyrus-main-app.c

  GYRUS -- GNOME Cyrus Administrator Main Application Class.

  Copyright (C) 2003-2004 Alejandro Valdes J.
  Copyright (C) 2003-2004 Jorge Bustos B.
  Copyright (C) 2003-2004 Claudio Saavedra V.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, see <http://www.gnu.org/licenses/>.
  
*/

#include <config.h>

#include <amtk/amtk.h>
#include <stdlib.h>

#include "glib/gi18n.h"
#include "gyrus-dialog-mailbox-new.h"
#include "gyrus-dialog-find-mailbox.h"
#include "gyrus-main-app.h"
#include "gyrus-session.h"
#include "gyrus-admin-mailbox.h"
#include "gyrus-report.h"

/*** Gyrus Main Application Private Data ***/
struct _GyrusMainAppPrivate {
        AmtkActionInfoStore *info_store;
        GtkWidget *notebook;
        GtkWidget *window;
};

G_DEFINE_TYPE_WITH_PRIVATE (GyrusMainApp, gyrus_main_app, GTK_TYPE_APPLICATION)

static void gyrus_main_app_about (void);

/*** Class Callbacks ***/

static void
gyrus_main_app_on_file_open_session (GSimpleAction *action,
				     GVariant *param,
				     gpointer user_data)
{
	gyrus_session_show_sessions (NULL, user_data);
}

static void
gyrus_main_app_on_file_exit (GSimpleAction *action,
			     GVariant *param,
			     gpointer user_data)
{
	g_application_quit (G_APPLICATION (user_data));
}

static void
gyrus_main_app_on_edit_find (GSimpleAction *action,
			     GVariant *param,
			     gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);

	GtkWidget *dialog =
		gyrus_dialog_find_mailbox_new ();
	
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
	                              GTK_WINDOW (app->priv->window));
	gyrus_dialog_find_mailbox_set_admin (GYRUS_DIALOG_FIND_MAILBOX (dialog),
					     gyrus_main_app_get_current_admin (app));
	
	gtk_widget_show (dialog);
}

static void
gyrus_main_app_on_edit_mailbox_add (GSimpleAction *action,
				    GVariant *param,
				    gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);

	GtkWidget *dialog =
		gyrus_dialog_mailbox_new_new ();
	gtk_widget_show (dialog);
	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (app->priv->window));
	
}

static void
gyrus_admin_delete_dialog_on_response (GtkDialog *dialog,
				       gint result,
				       gpointer user_data)
{
	GyrusAdmin * admin = GYRUS_ADMIN (user_data);

	if (result == GTK_RESPONSE_YES) {
		gchar *mailbox = gyrus_admin_get_selected_mailbox (admin);
		gyrus_admin_mailbox_delete_all (admin, mailbox);
		gyrus_admin_refresh_users_list (admin);
		gyrus_admin_mailbox_clear_info (admin);
		g_free (mailbox);
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
gyrus_main_app_on_edit_mailbox_remove (GSimpleAction *action,
				       GVariant *param,
				       gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);

	gchar *mailbox = gyrus_admin_get_selected_mailbox (admin);

	GtkWidget *dialog = gtk_message_dialog_new
		(GTK_WINDOW (app->priv->window), GTK_DIALOG_DESTROY_WITH_PARENT,
		 GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO,
		 _("Really delete mailbox '%s' and all of its submailboxes?"),
		 mailbox);
	
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (gyrus_admin_delete_dialog_on_response),
			  admin);
	
	gtk_dialog_run (GTK_DIALOG (dialog));
	
	g_free (mailbox);
}

static void
gyrus_main_app_on_acl_entry_delete (GSimpleAction *action,
				    GVariant *param,
				    gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);

	gyrus_admin_delete_selected_acl_entry (admin);
}

static void
gyrus_main_app_on_acl_entry_rename (GSimpleAction *action,
				    GVariant *param,
				    gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);

	gyrus_admin_start_editing_selected_acl (admin);
}

static void
gyrus_main_app_on_acl_entry_new (GSimpleAction *action,
				 GVariant *param,
				 gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);

	gyrus_admin_add_acl_entry (admin);
}

static void
gyrus_main_app_on_view_refresh (GSimpleAction *action,
                                GVariant *param,
				       gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin * admin = gyrus_main_app_get_current_admin (app);
	gyrus_admin_refresh_users_list (admin);
}

static void
gyrus_main_app_on_view_report (GSimpleAction *action,
                               GVariant *param,
				       gpointer user_data)
{
	GyrusMainApp *app  = GYRUS_MAIN_APP (user_data);
	GyrusAdmin *admin = gyrus_main_app_get_current_admin (app);

	gyrus_report_show_report(admin);
}

static void
gyrus_main_app_on_help_about (GSimpleAction *action,
                              GVariant *param,
				       gpointer user_data)
{
	gyrus_main_app_about();
}

static const char *server_actions[] = {
        "addmbox", "find", "refresh", "report", NULL
};
static const char *mbox_actions[] = {
        "new", "rmmbox", NULL
};
static const char *acl_actions[] = {
        "rename", "delete", NULL
};

static void
enable_actions (GyrusMainApp *app,
	        const char **actions,
	        gboolean enabled)
{
	GAction *action;
	guint i;

	for (i = 0; i < g_strv_length ((gchar **) actions); i++) {
	        action = g_action_map_lookup_action (G_ACTION_MAP (app),
	                                             actions[i]);
	        g_simple_action_set_enabled (G_SIMPLE_ACTION (action),
	                                     enabled);
	}
}

static void
gyrus_main_app_make_sensitivity_consistent (GyrusMainApp *app)
{
	gint n_page;
	GyrusAdmin *admin;

	n_page = gtk_notebook_get_current_page
		(GTK_NOTEBOOK (app->priv->notebook));

	/* If there are no pages left in Notebook */
	if (n_page < 0) {
	        enable_actions (app, server_actions, FALSE);
	        enable_actions (app, mbox_actions, FALSE);
	        enable_actions (app, acl_actions, FALSE);
		return;
	}
	
	admin = GYRUS_ADMIN
		(gtk_notebook_get_nth_page (GTK_NOTEBOOK (app->priv->notebook),
					    n_page));
	
	if (gyrus_admin_is_connected (admin)) {
	        enable_actions (app, server_actions, TRUE);
	        enable_actions (app, acl_actions,
	                        gyrus_admin_acl_has_selection (admin));
	}
	else
	        enable_actions (app, server_actions, FALSE);
}

static void
gyrus_main_app_on_button_close_page_clicked (GtkButton *button, gpointer data)
{
	gint page_number;
	GtkWidget    *admin   = GTK_WIDGET (data);
	gpointer     app_data = g_object_get_data (G_OBJECT (button),
						   "parent-app");
	GyrusMainApp *app     = GYRUS_MAIN_APP (app_data);
	
	page_number = gtk_notebook_page_num (GTK_NOTEBOOK
					     (app->priv->notebook), admin);
	gtk_notebook_remove_page (GTK_NOTEBOOK
				  (app->priv->notebook), page_number);

	gyrus_main_app_make_sensitivity_consistent (app);
	
	if (gtk_notebook_get_n_pages (GTK_NOTEBOOK (app->priv->notebook)) == 0)
		gtk_window_set_title (GTK_WINDOW (app->priv->window),
	                              _("Cyrus IMAP Administrator"));
}

static void
gyrus_main_app_on_switch_page (GtkNotebook *notebook,
			       gpointer *page,
			       guint page_num,
			       gpointer user_data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (user_data);
	GyrusAdmin *admin = GYRUS_ADMIN (gtk_notebook_get_nth_page (notebook,
								    page_num));
	
	const gchar *session_name = gyrus_admin_get_current_session_name (admin);
	gchar *title = g_strdup_printf (_("%s - Cyrus IMAP Administrator"), 
					session_name);
	gchar *mailbox;
	
	gtk_window_set_title (GTK_WINDOW (app->priv->window), title);
	g_free (title);


	if (gyrus_admin_is_connected (admin))
	        enable_actions (app, server_actions, TRUE);
	else
	        enable_actions (app, server_actions, FALSE);

	if ((mailbox = gyrus_admin_get_selected_mailbox (admin)) != NULL) {
	        enable_actions (app, mbox_actions, TRUE);
		if (gyrus_admin_has_current_acl_access (admin) == FALSE) {
			/* TODO: entry_new to false */ ;
		}
		g_free (mailbox);
	}
	else {
	        enable_actions (app, mbox_actions, FALSE);
	}
	enable_actions (app, acl_actions,
	                gyrus_admin_acl_has_selection (admin));
}

const AmtkActionInfoEntry acl_entries[] = {
        { "app.new", NULL, N_("New entry"), "<Control><Shift>n",
          N_("Create a new ACL entry in current mailbox") },
        { "app.rename", NULL, N_("Rename entry"),
          "<Control><Shift>r", N_("Rename selected ACL entry") },
        { "app.delete", NULL, N_("Delete entry"),
          "<Control><Shift>d", N_("Delete selected ACL entry") },
        { NULL }
};

static void
gyrus_main_app_startup (GApplication *application)
{
	const AmtkActionInfoEntry amtk_entries[] = {
	        { "app.open", "document-open", N_("Go to server..."),
	          "<Control>o", N_("Show the list of servers") },
	        { "app.quit", "application-exit", N_("_Quit"),
	          "<Control>q" },
	        { "app.find", "edit-find", N_("_Find"), "<Control>f",
	          N_("Search for a mailbox in current server") },
	        { "app.refresh", "view-refresh", N_("_Refresh"),
	          "<Control>r", N_("Refresh the mailbox list") },
	        { "app.report", NULL, N_("Create report..."), NULL,
	          N_("Create report of users with quota problems") },
	        { "app.about", "help-about", N_("_About") },
	        { "app.addmbox", "list-add", N_("Add mailbox"),
	          NULL, N_("Add a mailbox under the one selected") },
	        { "app.rmmbox", "list-remove", N_("Remove mailbox"),
	          NULL, N_("Remove current mailbox from the server") },
	        { NULL }
	};

	const GActionEntry entries[] = {
	        { "open", gyrus_main_app_on_file_open_session },
	        { "quit", gyrus_main_app_on_file_exit },
	        { "find", gyrus_main_app_on_edit_find },
	        { "new", gyrus_main_app_on_acl_entry_new },
	        { "rename", gyrus_main_app_on_acl_entry_rename },
	        { "delete", gyrus_main_app_on_acl_entry_delete },
	        { "refresh", gyrus_main_app_on_view_refresh },
	        { "report", gyrus_main_app_on_view_report },
	        { "about", gyrus_main_app_on_help_about },
	        { "addmbox", gyrus_main_app_on_edit_mailbox_add },
	        { "rmmbox", gyrus_main_app_on_edit_mailbox_remove },
	        { NULL }
	};

	GyrusMainApp *app = GYRUS_MAIN_APP (application);

	if (G_APPLICATION_CLASS (gyrus_main_app_parent_class)->startup)
	        G_APPLICATION_CLASS (gyrus_main_app_parent_class)->startup
	                (application);

	g_assert (app->priv->info_store == NULL);
	app->priv->info_store = amtk_action_info_store_new ();

	amtk_action_info_store_add_entries (app->priv->info_store,
	                                    amtk_entries, -1,
	                                    GETTEXT_PACKAGE);
	amtk_action_info_store_add_entries (app->priv->info_store,
	                                    acl_entries, -1,
	                                    GETTEXT_PACKAGE);
	amtk_action_map_add_action_entries_check_dups (G_ACTION_MAP (app),
	                                               entries, -1,
	                                               app);
}

static void
gyrus_main_app_activate (GApplication *application)
{

	AmtkFactory *factory;
	AmtkApplicationWindow *win;
	GtkWidget *main_vbox;
	GtkWidget *menubar;
	GtkWidget *menu;
	GtkWidget *submenu;
	GtkWidget *toolbar;
	GtkWidget *statusbar;

	GyrusMainApp *app = GYRUS_MAIN_APP (application);

	app->priv->window = gtk_application_window_new (GTK_APPLICATION
	                                                (application));
	gtk_window_set_default_size (GTK_WINDOW (app->priv->window),
	                             600, 400);
	gtk_window_set_title (GTK_WINDOW (app->priv->window),
			      _("Cyrus IMAP Administrator"));

	main_vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);

	factory = amtk_factory_new_with_default_application ();

	menubar = gtk_menu_bar_new ();

	menu = gtk_menu_item_new_with_mnemonic (_("_File"));
	submenu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu), submenu);
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu),
	                       amtk_factory_create_menu_item (factory,
	                                                      "app.open"));
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu),
	                       gtk_separator_menu_item_new ());
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu),
	                       amtk_factory_create_menu_item (factory,
	                                                      "app.quit"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menu);

	menu = gtk_menu_item_new_with_mnemonic (_("_Edit"));
	submenu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu), submenu);
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu),
	                       amtk_factory_create_menu_item (factory,
	                                                      "app.find"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menu);

	menu = gtk_menu_item_new_with_mnemonic (_("_ACL"));
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu),
	                           amtk_factory_create_simple_menu (factory,
	                                                            acl_entries,
	                                                            -1));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menu);

	menu = gtk_menu_item_new_with_mnemonic (_("View"));
	submenu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu), submenu);
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu),
	                       amtk_factory_create_menu_item (factory,
                                                              "app.refresh"));
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu),
	                       gtk_separator_menu_item_new ());
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu),
	                       amtk_factory_create_menu_item (factory,
	                                                      "app.report"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menu);

	menu = gtk_menu_item_new_with_mnemonic (_("_Help"));
	submenu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu), submenu);
	gtk_menu_shell_append (GTK_MENU_SHELL (submenu),
	                       amtk_factory_create_menu_item (factory,
	                                                      "app.about"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menubar), menu);

	gtk_box_pack_start (GTK_BOX (main_vbox), menubar, FALSE, FALSE, 0);

	toolbar = gtk_toolbar_new ();
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar),
	                    amtk_factory_create_tool_button (factory,
	                                                     "app.open"),
	                    -1);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar),
	                    gtk_separator_tool_item_new (), -1);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar),
	                    amtk_factory_create_tool_button (factory,
	                                                     "app.addmbox"),
	                    -1);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar),
	                    amtk_factory_create_tool_button (factory,
	                                                     "app.rmmbox"),
	                    -1);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar),
	                    gtk_separator_tool_item_new (), -1);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar),
	                    amtk_factory_create_tool_button (factory,
	                                                     "app.find"),
	                    -1);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar),
	                    gtk_separator_tool_item_new (), -1);
	gtk_toolbar_insert (GTK_TOOLBAR (toolbar),
	                    amtk_factory_create_tool_button (factory,
	                                                     "app.refresh"),
	                    -1);

	gtk_box_pack_start (GTK_BOX (main_vbox), toolbar, FALSE, FALSE, 0);
	g_object_unref (factory);

	app->priv->notebook = gtk_notebook_new ();

	g_signal_connect (G_OBJECT (app->priv->notebook), "switch-page",
			  G_CALLBACK (gyrus_main_app_on_switch_page), app);
	
	gtk_box_pack_start (GTK_BOX (main_vbox), app->priv->notebook,
			    TRUE, TRUE, 0);

	statusbar = gtk_statusbar_new ();
	win = amtk_application_window_get_from_gtk_application_window
	        (GTK_APPLICATION_WINDOW (app->priv->window));
	amtk_application_window_set_statusbar (win, GTK_STATUSBAR (statusbar));
	amtk_application_window_connect_menu_to_statusbar (win,
	                                                   GTK_MENU_SHELL
	                                                   (menubar));
	gtk_box_pack_start (GTK_BOX (main_vbox), statusbar, FALSE, FALSE, 0);

	gtk_container_add (GTK_CONTAINER (app->priv->window), main_vbox);

	gtk_widget_show_all (app->priv->window);

	gyrus_main_app_make_sensitivity_consistent (app);
	amtk_action_info_store_check_all_used (app->priv->info_store);
}

/*** Private Methods ***/

static void
gyrus_main_app_dispose (GObject *object)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (object);

	g_clear_object (&app->priv->info_store);

	G_OBJECT_CLASS (gyrus_main_app_parent_class)->dispose (object);
}

static void
gyrus_main_app_class_init (GyrusMainAppClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GApplicationClass *app_class = G_APPLICATION_CLASS (class);

	gyrus_main_app_parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gyrus_main_app_dispose;

	app_class->startup = gyrus_main_app_startup;
	app_class->activate = gyrus_main_app_activate;
}

static void
gyrus_main_app_init (GyrusMainApp *app)
{
	/* get a private struct */
	app->priv = gyrus_main_app_get_instance_private (app);

	g_set_application_name (_("Cyrus IMAP Administrator"));
}

GyrusAdmin *
gyrus_main_app_get_current_admin (GyrusMainApp *app)
{
	GyrusAdmin *admin;
	gint n;
	n = gtk_notebook_get_current_page (GTK_NOTEBOOK (app->priv->notebook));
	admin = GYRUS_ADMIN (gtk_notebook_get_nth_page
			     (GTK_NOTEBOOK (app->priv->notebook),
			      n));

	return admin;
}

static void
gyrus_main_app_about (void)
{
	GdkPixbuf *pixbuf;
//	gchar *logo_gyrus;

	const gchar *authors[] = {
		"Alejandro Vald" "\xC3\xA9" "s Jim" "\xC3\xA9"
		"nez <avaldes@utalca.cl>",
		"Jorge Bustos Bustos <jbustos@utalca.cl>",
		"Claudio Saavedra "
		"<csaavedra@igalia.com>",
		"Francisco Rojas <frojas@alumnos.utalca.cl>",
		NULL
	};

	const gchar *translators = _("translators-credits");

//	logo_gyrus = g_strdup_printf ("%s%c%s", GYRUS_PIXMAPS_DIR,
//	                              G_DIR_SEPARATOR, "logo_gyrus.png");
	                              
//	pixbuf = gdk_pixbuf_new_from_file(logo_gyrus, NULL);
	pixbuf = gdk_pixbuf_new_from_resource ("/org/gnome/gyrus/logo_gyrus.png", NULL);

	
//	g_free (logo_gyrus);
	
	gtk_show_about_dialog (NULL,
			       "name", _("GNOME Cyrus Administrator"),
			       "version", VERSION,
			       "copyright", _("(c) 2003-2005 GNOME Foundation\n"
					      "(c) 2004-2005 Claudio Saavedra"),
			       "comments", _("Administration tool for Cyrus IMAP servers."),
			       "authors", authors, 
			       "translator-credits", strcmp (translators, "translators-credits") != 0 ? translators : NULL,
			       "logo", pixbuf,
			       "website", "https://www.gitlab.com/wvdakker/gyrus",
			       NULL);

	g_object_unref(pixbuf);
}

static GtkWidget *
gyrus_main_app_create_label_from_admin (GyrusMainApp *app, GyrusAdmin *admin)
{
	GtkWidget *hbox;
	GtkWidget *image;
	GtkWidget *button;
	GtkWidget *label;

	const gchar *session_name;
	
	/* create a text label */
	session_name = gyrus_admin_get_current_session_name (admin);
	label = gtk_label_new (session_name ? session_name : "");
	
	/* create a button with a close icon */
	image = gtk_image_new_from_icon_name ("window-close",
	                                      GTK_ICON_SIZE_MENU);
	button = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	gtk_widget_set_size_request (button, 20, 20);
	gtk_container_add (GTK_CONTAINER (button), image);
	g_object_set_data (G_OBJECT (button), "parent-app", app);

	/* create the container for both widgets*/
	hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	gtk_widget_show_all (hbox);

	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (gyrus_main_app_on_button_close_page_clicked),
			  admin);

	return hbox;
}

/*** Public Methods ****/
GyrusMainApp *
gyrus_main_app_new (void)
{
	GyrusMainApp *app;
	app = g_object_new (GYRUS_TYPE_MAIN_APP,
	                    "application-id", "org.gnome.gyrus",
	                    "flags", G_APPLICATION_FLAGS_NONE,
	                    NULL);
	return app;
}

static void
main_app_on_admin_connected (GyrusAdmin *admin,
			     gpointer *data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (data);
	enable_actions (app, server_actions, TRUE);
}

static void
main_app_on_admin_disconnected (GyrusAdmin *admin,
				gpointer *data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (data);
	enable_actions (app, server_actions, FALSE);
	enable_actions (app, mbox_actions, FALSE);
}

static void
main_app_on_acl_selection_changed (GyrusAdmin *admin,
				   gboolean selected,
				   gpointer *data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (data);
	enable_actions (app, acl_actions, selected);
}

static void
main_app_on_mailbox_selection_changed (GyrusAdmin *admin,
				       gboolean selected,
				       gpointer *data)
{
	GyrusMainApp *app = GYRUS_MAIN_APP (data);
	enable_actions (app, mbox_actions, selected);
}

GtkWidget *
gyrus_main_app_append_page (GyrusMainApp *app, GyrusSession *session)
{
	GtkWidget *hbox_label;
	gint page_num;
	GtkWidget *admin;

	g_return_val_if_fail (GYRUS_IS_MAIN_APP (app), NULL);

	admin = gyrus_admin_new (session);

	g_signal_connect (G_OBJECT (admin), "connected", 
			  G_CALLBACK (main_app_on_admin_connected), 
			  app);

	g_signal_connect (G_OBJECT (admin), "disconnected", 
			  G_CALLBACK (main_app_on_admin_disconnected), 
			  app);
	
	g_signal_connect (G_OBJECT (admin), "acl-selection-changed", 
			  G_CALLBACK (main_app_on_acl_selection_changed), 
			  app);

	g_signal_connect (G_OBJECT (admin), "mailbox-selection-changed", 
			  G_CALLBACK (main_app_on_mailbox_selection_changed), 
			  app);
	
	gtk_notebook_append_page (GTK_NOTEBOOK (app->priv->notebook),
				  admin, NULL);
	
	hbox_label = gyrus_main_app_create_label_from_admin
		(app, GYRUS_ADMIN (admin));
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (app->priv->notebook),
				    admin, hbox_label);

	page_num = gtk_notebook_page_num (GTK_NOTEBOOK (app->priv->notebook),
					  admin);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (app->priv->notebook),
				       page_num);
	
	return admin;
}

void
gyrus_main_app_menu_set_sensitive (GyrusMainApp *app, gboolean sens)
{
	g_return_if_fail (GYRUS_IS_MAIN_APP (app));
	enable_actions (app, server_actions, sens);
}

/***** Above functions doesnt belong this file ******/

int
main (int argc, char *argv[])
{
	GyrusMainApp *app;
	int status;

	bindtextdomain (GETTEXT_PACKAGE, GNOME_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	amtk_init ();

	app = gyrus_main_app_new ();
	status = g_application_run (G_APPLICATION (app), argc, argv);

	amtk_finalize ();
	g_object_unref (app);

	return status;
}
